! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Treasure hunt
! http://users.csc.calpoly.edu/~jdalbey/103/Projects/ProgrammingPractice.html#easy - Easy 1

program treasure_hunt
  implicit none
  integer,dimension(:,:),allocatable :: map
  
  print*,'====================================='
  print*,'Programming Practice - Treasure Hunt!'
  print*,'====================================='
  call init
  call hunt
  
contains

  ! Create map
  subroutine init
    implicit none
    integer :: n

    n = 5
    allocate(map(n,n))

    open(21, file="map.txt")
    read(21,*) map
    map = transpose(map)
    
    return
  end subroutine init

  ! Hunt for treasure
  subroutine hunt
    implicit none
    integer :: num,count
    integer,dimension(2) :: pos,npos
    
    pos = (1,1)
    num = pos(1)*10+pos(2)
    count = 0
    do
       if (num.eq.map(pos(1),pos(2))) then
          print*,'Found it!'
          print*,'I''m at',num,'and my number is',map(pos(1),pos(2))
          exit
       else
          count = count + 1
          print*,'I''m at',num,'but my number is',map(pos(1),pos(2)),count
          npos(1) = map(pos(1),pos(2))/10
          npos(2) = mod(map(pos(1),pos(2)),10)
          pos = npos
          num = pos(1)*10+pos(2)
       end if
    end do
    
    return
  end subroutine hunt
  
end program treasure_hunt
